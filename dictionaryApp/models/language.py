from django.db import models
from django.db.models.deletion import  PROTECT
from django.db.models.fields import AutoField, CharField, TextField

from .user import User

class Language(models.Model):
    id = models.AutoField(primary_key=True)
    Name = models.TextField("Name", max_length=100)
    numberSpeaker = models.IntegerField("numberSpeaker")
    creation_date = models.DateTimeField(null=True)
    last_modification = models.DateTimeField(null=True)