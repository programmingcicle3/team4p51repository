from django.db import models
from django.db.models.deletion import PROTECT
from django.db.models.fields import TextField
from django.utils import timezone
from .language import Language

class Tribe(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField("name",max_length=100)
    language = models.ForeignKey(Language,related_name="language", on_delete=PROTECT)
    ubication = models.TextField("ubication", max_length=300)
    image = models.TextField("image", max_length=300)
    alternative_names = models.TextField(max_length=200)
    creation_date = models.DateTimeField(null=True)
    last_modification = models.DateTimeField(null=True)