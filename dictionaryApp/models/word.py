from typing import cast
from django.db import models
from django.db.models.deletion import  PROTECT
from django.db.models.fields import AutoField, CharField, TextField
from django.db.models.fields.related import ForeignKey

from .tribe import Tribe
from .user import User
from .wordType import WordType

class Word(models.Model):
    id = AutoField(primary_key=True)
    tribe =  ForeignKey(Tribe,related_name="tribe",on_delete=PROTECT)
    Word = models.CharField("word",max_length=30)
    word_type = models.ForeignKey(WordType,related_name="Word_Type", on_delete=PROTECT)
    morfo = TextField("morfo",max_length=100)
    morfema = TextField("morfema",max_length=100)
    translation = TextField("translation",max_length=100)
    lexical_translation = TextField("lexical_translation", max_length=300)
    observations = TextField("observations",max_length=300)
    user_responsible = ForeignKey(User,related_name="user_responsible", on_delete=PROTECT)
    creation_date = models.DateTimeField(null=True)
    last_modification = models.DateTimeField(null=True)


