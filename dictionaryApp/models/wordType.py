from django.db import models
from django.db. models import Model
from django.db.models.fields import AutoField, CharField
from django.utils import timezone

class WordType(models.Model):
    id = AutoField(primary_key=True),
    name = CharField("name",max_length=50)
    creation_date = models.DateTimeField(null=True)
    last_modification = models.DateTimeField(null=True)