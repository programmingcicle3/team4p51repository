from dictionaryApp.models.language import Language
from rest_framework import serializers
import datetime

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'

    def create(self, validated_data):
        validated_data["creation_date"] = datetime.datetime.now()
        LanguageInstance = Language.objects.create(**validated_data)
        return LanguageInstance
    
    def update(self, languageInstance, validated_data):   
        
        languageInstance.Name = validated_data.get('Name',languageInstance.Name)
        languageInstance.numberSpeaker = validated_data.get('numberSpeaker',languageInstance.numberSpeaker)               
        languageInstance.last_modification = datetime.datetime.now()

        languageInstance.save()

        return languageInstance        