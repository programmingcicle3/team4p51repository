from django.conf.urls import url
from rest_framework import serializers
from dictionaryApp.models.tribe import Tribe
from dictionaryApp.models import Language
import datetime

class TribeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tribe
        fields = '__all__'
        
    def create(self, validated_data):
        validated_data["creation_date"] = datetime.datetime.now()
        tribeInstance = Tribe.objects.create(**validated_data)
        return tribeInstance
    
    def update(self, tribeInstance, validated_data):   

        tribeInstance.name = validated_data.get('name',tribeInstance.name)
        tribeInstance.language = validated_data.get('language',tribeInstance.language)
        tribeInstance.ubication = validated_data.get('ubication',tribeInstance.ubication)
        tribeInstance.image = validated_data.get('image',tribeInstance.image)
        tribeInstance.alternative_names = validated_data.get('alternative_names',tribeInstance.alternative_names)
        tribeInstance.last_modification = datetime.datetime.now()  

        tribeInstance.save()
        return tribeInstance

    def to_representation(self, obj):
        tribe = Tribe.objects.get(id=obj.id)
        language = Language.objects.get(id=obj.language.id)
        return{
            'id': tribe.id,
            'name': tribe.name,
            'ubication': tribe.ubication,
            'image' : tribe.image,
            'alternative_names': tribe.alternative_names,
            'language': {
                'id': language.id,
                'name': language.Name,
                'description': language.numberSpeaker, 
            }      
        }
    
    
