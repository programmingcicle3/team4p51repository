from dictionaryApp.models import tribe, word
from dictionaryApp.models.user import User
from dictionaryApp.models.word import Word
from rest_framework import serializers
import datetime

from dictionaryApp.models.wordType import WordType

class WordSerializer(serializers.ModelSerializer):

    class Meta:
        model = Word
        fields = '__all__'

    def create(self, validated_data):
        validated_data["creation_date"] = datetime.datetime.now()
        WordInstance = Word.objects.create(**validated_data)

        return WordInstance
    
    def update(self, wordInstance, validated_data):   

        wordInstance.tribe = validated_data.get('tribe',wordInstance.tribe)
        wordInstance.Word = validated_data.get('Word',wordInstance.Word)
        wordInstance.word_type = validated_data.get('word_type',wordInstance.word_type)
        wordInstance.morfo = validated_data.get('morfo',wordInstance.morfo)
        wordInstance.morfema = validated_data.get('morfema',wordInstance.morfema)
        wordInstance.translation = validated_data.get('translation',wordInstance.translation)
        wordInstance.lexical_translation = validated_data.get('lexical_translation',wordInstance.lexical_translation)
        wordInstance.observations = validated_data.get('observations',wordInstance.observations)
        wordInstance.user_responsible = validated_data.get('user_responsible',wordInstance.user_responsible)
        wordInstance.last_modification = datetime.datetime.now()   

        wordInstance.save()
        return wordInstance
         

    def to_representation(self, obj):
    
        wordInstance = Word.objects.get(id = obj.id)
        tribeInstance = tribe.Tribe.objects.get(id = obj.tribe.id)
        wordTypeInstance = WordType.objects.get(id = obj.word_type.id)
        userInstance = User.objects.get(id= obj.user_responsible.id)
        
       
        return{
            "id": wordInstance.id,
            "tribe": {
                        "id": tribeInstance.id,
                        "name": tribeInstance.name
                    },
            "Word": wordInstance.Word,
            "word_type": {
                        "id": wordTypeInstance.id,
                        "name": wordTypeInstance.name
                    },
            "morfo": wordInstance.morfo,
            "morfema": wordInstance.morfema,
            "translation": wordInstance.translation,
            "lexical_translation": wordInstance.lexical_translation,
            "observations": wordInstance.observations,
            "user_responsible": {
                        "id": userInstance.id,
                        "name": userInstance.name
                    },
        }


    
