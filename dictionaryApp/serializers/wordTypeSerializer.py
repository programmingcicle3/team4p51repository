from dictionaryApp.models.wordType import WordType
from rest_framework import serializers
import datetime

class WordTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = WordType
        fields = ['id', 'name', 'creation_date', 'last_modification']

    def create(self, validated_data):
        validated_data["creation_date"] = datetime.datetime.now()
        WordTypeInstance = WordType.objects.create(**validated_data)

        return WordTypeInstance
    
    def update(self, wordTypeInstance, validated_data):   
        
        wordTypeInstance.name = validated_data.get('name',wordTypeInstance.name)        
        wordTypeInstance.last_modification = datetime.datetime.now() 

        wordTypeInstance.save()
        return wordTypeInstance