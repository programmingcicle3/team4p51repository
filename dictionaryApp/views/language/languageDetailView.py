
from dictionaryApp.models.language import Language
from dictionaryApp.serializers import LanguageSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated


class LanguageDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            if  Functions_Global.is_Authenticated(request):
                model_language = Language.objects.filter(id=kwargs['pk']).first()
                serializer = LanguageSerializer(model_language)
                return Response(serializer.data)

        except Exception as ex:
            return Response(str(ex), status=status.HTTP_502_BAD_GATEWAY)

    def put(self, request,*args, **kwargs):
        try:
            if Functions_Global.is_Authenticated(request):
                model_language = Language.objects.filter(id=kwargs['pk']).first()
                serializer=LanguageSerializer(model_language, data=request.data)
                serializer.is_valid()
                serializer.save()

                return Response(serializer.data, status= status.HTTP_200_OK)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)

    def delete(self, request,*args, **kwargs):
        try:
            if Functions_Global.is_Authenticated(request):
                model_language = Language.objects.filter(id=kwargs['pk']).first()
                model_language.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)
