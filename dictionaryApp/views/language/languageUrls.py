from django.urls import path
from dictionaryApp.views import language

urlpatterns = [
    path('list/', language.LanguageListView.as_view()),
    path('list/<int:pk>', language.LanguageDetailView.as_view()),
]
