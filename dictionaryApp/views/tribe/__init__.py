from .tribeCreateView import TribeCreateView
from .tribeListAllView import TribeListAllView
from .tribeDetailsView import TribeDetailsView
from .tribeUpdateView import TribeUpdateView
from .tribeDeleteView import TribeDeleteView