from rest_framework import status, views
from rest_framework.response import Response
from dictionaryApp.models.tribe import Tribe
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated

from dictionaryApp.serializers.tribeSerializer import TribeSerializer


class TribeCreateView(views.APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
    
        try:
            
            if  Functions_Global.is_Authenticated(request):
                serializer = TribeSerializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                tribeInstance = serializer.save()
                tribe = Tribe.objects.filter(id = tribeInstance.id).first()
                tribeSerializer = TribeSerializer(tribe)

                return Response(tribeSerializer.data, status=status.HTTP_201_CREATED)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)

       