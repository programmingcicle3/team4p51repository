from rest_framework import status, views
from rest_framework.response import Response
from dictionaryApp.models.tribe import Tribe
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated



class TribeDeleteView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            if  Functions_Global.is_Authenticated(request):
                tribe = Tribe.objects.filter(id = kwargs['pk']).first()
                tribe.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)

        
        