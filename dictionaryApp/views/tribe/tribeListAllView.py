from rest_framework import status, views
from rest_framework.response import Response
from dictionaryApp.models.tribe import Tribe
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated

from dictionaryApp.serializers import TribeSerializer

class TribeListAllView(views.APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        try:
            if  Functions_Global.is_Authenticated(request):
                tribe = Tribe.objects.all()
                tribeSerializer = TribeSerializer(tribe, many= True)
                return Response(tribeSerializer.data, status = status.HTTP_200_OK)
            
        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)
        