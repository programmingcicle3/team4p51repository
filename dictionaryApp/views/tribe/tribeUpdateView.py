from rest_framework import status, views
from rest_framework.response import Response
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated

from dictionaryApp.serializers.tribeSerializer import TribeSerializer
from dictionaryApp.models.tribe import Tribe

class TribeUpdateView(views.APIView):
    permission_classes = (IsAuthenticated,)
    def put(self, request, *args, **kwargs):
        try:
            if  Functions_Global.is_Authenticated(request):
                tribeInstance = Tribe.objects.filter(id=kwargs['pk']).first()
                serializer = TribeSerializer(tribeInstance, data=request.data)
                serializer.is_valid(raise_exception=True)
                tribeInstance = serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)
          