from django.urls import path
from dictionaryApp.views import tribe

urlpatterns = [
    path('list/', tribe.TribeListAllView.as_view()),
    path('list/<int:pk>', tribe.TribeDetailsView.as_view()),
    path('create/', tribe.TribeCreateView.as_view()),
    path('update/<int:pk>', tribe.TribeUpdateView.as_view()),
    path('delete/<int:pk>', tribe.TribeDeleteView.as_view()),
]