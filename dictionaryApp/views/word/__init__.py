from .wordCreateView import WordCreateView
from .wordListAllView import WordListAllView
from .wordDetailsView import WordDetailsView
from .wordUpdateView import WordUpdateView
from .wordDeleteView import WordDeleteView