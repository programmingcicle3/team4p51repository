from dictionaryApp.models.wordType import WordType
from dictionaryApp.serializers import WordTypeSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated

class WordTypeDetailView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):

        try:
            if  Functions_Global.is_Authenticated(request):
                model_wordType = WordType.objects.filter(id=kwargs['pk']).first()
                serializer = WordTypeSerializer(model_wordType)
                return Response(serializer.data)
        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY) 

    def put(self, request, *args, **kwargs):

        try:
            if  Functions_Global.is_Authenticated(request):
                model_wordType = WordType.objects.filter(id=kwargs['pk']).first()
                serializer = WordTypeSerializer(model_wordType, data=request.data)
                serializer.is_valid()
                serializer.save()
                return Response(serializer.data)
        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)

    def delete(self, request, *args, **kwargs):

        try:
            if  Functions_Global.is_Authenticated(request):
                model_wordType = WordType.objects.filter(id=kwargs['pk']).first()
                model_wordType.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)
        