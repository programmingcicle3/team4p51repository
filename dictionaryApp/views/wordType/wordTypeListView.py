﻿from dictionaryApp.models.wordType import WordType
from dictionaryApp.serializers import WordTypeSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dictionaryApp.DictionaryUtils import Functions_Global
from rest_framework.permissions import IsAuthenticated

class WordTypeListView(APIView):

    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        try:
            if  Functions_Global.is_Authenticated(request):
                model_wordType = WordType.objects.all()        
                serializer = WordTypeSerializer(model_wordType, many=True)
                return Response(serializer.data)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)
        

    def post(self, request, *args, **kwargs):

        try:
            if  Functions_Global.is_Authenticated(request):
                serializer = WordTypeSerializer(data=request.data)
                serializer.is_valid()
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        except Exception as ex:
            return Response(str(ex), status = status.HTTP_502_BAD_GATEWAY)