from django.urls import path
from dictionaryApp.views import wordType

urlpatterns = [
    path('list/', wordType.WordTypeListView.as_view()),
    path('list/<int:pk>', wordType.WordTypeDetailView.as_view()),
]
